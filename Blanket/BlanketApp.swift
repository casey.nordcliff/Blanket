import SwiftUI

@main
struct BlanketApp: App {
    @State var players = AudioManager()
    var body: some Scene {
        WindowGroup {
            ContentView()
                .frame(minWidth: 250, maxWidth: .infinity, minHeight: 250)
                .environmentObject(players)
        }
    }
}
