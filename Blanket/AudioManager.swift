import SwiftUI
import AVFAudio

final class AudioManager: ObservableObject {
    
    var players: [String: AVAudioPlayer] = [:]
    
    func generateAVPlayer(fileName: String) -> AVAudioPlayer {
        let path = Bundle.main.path(forResource: fileName, ofType: ".mp3")!
        let url = URL(fileURLWithPath: path)
        let player = try! AVAudioPlayer(contentsOf: url)
        player.prepareToPlay()
        player.volume = 0.0
        player.numberOfLoops = -1
        return player
    }
    
    init() {
        players = [
            "Rain": generateAVPlayer(fileName: "rain"),
            "Storm": generateAVPlayer(fileName: "storm"),
            "Wind": generateAVPlayer(fileName: "wind"),
            "Waves": generateAVPlayer(fileName: "waves"),
            "Stream": generateAVPlayer(fileName: "stream"),
            "Birds": generateAVPlayer(fileName: "birds"),
            "Summer Night": generateAVPlayer(fileName: "summernight"),
            "Train": generateAVPlayer(fileName: "train"),
            "Boat": generateAVPlayer(fileName: "boat"),
            "City": generateAVPlayer(fileName: "city"),
            "Coffee Shop": generateAVPlayer(fileName: "coffeeshop"),
            "Fireplace": generateAVPlayer(fileName: "fireplace"),
            "Pink Noise": generateAVPlayer(fileName: "pinknoise"),
            "White Noise": generateAVPlayer(fileName: "whitenoise")
        ]
    }
}
