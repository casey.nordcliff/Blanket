import SwiftUI
import UniformTypeIdentifiers

let icons = [
    "Rain": ["rain", "rain"],
    "Storm": ["storm", "storm"],
    "Wind": ["wind", "wind"],
    "Waves": ["waves", "waves"],
    "Stream": ["stream", "stream"],
    "Birds": ["bird", "bird"],
    "Summer Night": ["summernight", "summernight"],
    "Train": ["train", "train"],
    "Boat": ["boat", "boat"],
    "City": ["city", "city"],
    "Coffee Shop": ["coffeeshop", "coffeeshop"],
    "Fireplace": ["fireplace", "fireplace"],
    "Pink Noise": ["pinknoise", "pinknoise"],
    "White Noise": ["whitenoise", "whitenoise"]
]

struct Sound: View {
    var label: String
    @State var volume: Float = 0.0
    @State var picture: String?
    @EnvironmentObject var audioManager: AudioManager
    var body: some View {
        HStack {
            Image(picture ?? icons[label]![0])
                .resizable()
                .frame(width: 48.0, height: 48.0)
                .padding()
            VStack {
                Text(label).frame(maxWidth: .infinity, alignment: .bottomLeading)
                Slider(
                    value: Binding(
                        get: { self.volume },
                        set: { newVolume in
                            self.volume = newVolume
                            self.updateVolume()
                        }
                    ),
                    in: 0...200
                )
            }
        }
    }
    func updateVolume() {
        audioManager.players[label]!.volume = volume
        if volume == 0.0 {
            audioManager.players[label]!.pause()
            picture = icons[label]![0]
        }else {
            audioManager.players[label]!.play()
            picture = icons[label]![1]
        }
    }
}

struct SoundGroup: View {
    var text: String
    var sounds: [Sound]
    var body: some View {
        VStack {
            Text(text)
                .font(.title .bold())
                .frame(maxWidth: 600, alignment: .leading)
                .padding(.top, 16)
            GroupBox() {
                ForEach(0..<sounds.count, id: \.self) { i in
                    sounds[i]
                    if i < sounds.count - 1 { Divider() }
                }
            }
                .padding(.bottom, 16)
                .frame(maxWidth: 600, alignment: .center)
                .cornerRadius(4)
        }.frame(maxWidth: .infinity, alignment: .center)
    }
}

struct ContentView: View {
    var body: some View {
        ScrollView() {
            SoundGroup(text: "Nature", sounds: [
                Sound(label: "Rain"),
                Sound(label: "Storm"),
                Sound(label: "Wind"),
                Sound(label: "Waves"),
                Sound(label: "Stream"),
                Sound(label: "Birds"),
                Sound(label: "Summer Night")
            ])
            SoundGroup(text: "Travel", sounds: [
                Sound(label: "Train"),
                Sound(label: "Boat"),
                Sound(label: "City")
            ])
            SoundGroup(text: "Interiors", sounds: [
                Sound(label: "Coffee Shop"),
                Sound(label: "Fireplace")
            ])
            SoundGroup(text: "Noise", sounds: [
                Sound(label: "Pink Noise"),
                Sound(label: "White Noise")
            ])
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
