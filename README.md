# Blanket

Native MacOS Swift/SwiftUI clone of Blanket (https://github.com/rafaelmardojai/blanket), a GNOME Circle app that plays relaxing sounds.

## Known Issues

No activated icons for sounds > 0.0 in volume

No complete icon set

## Running / Building

1) Open the Blanket.xcodeproj file in Xcode
2) Command+R to build and run it
3) Product -> Show Build Folder In Finder to find the .app itself (it might be behind several folders)

This should work on both M1 and Intel macs